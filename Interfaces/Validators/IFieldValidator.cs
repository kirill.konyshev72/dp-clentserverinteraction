﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace DP.ClientServerInteraction.Common.Interfaces.Validators
{
    /// <summary>
    /// Validator of the specific field in the request.
    /// </summary>
    /// <typeparam name="T">Type of the field in the request model to validate. Can be simple or complex type.</typeparam>
    public interface IFieldValidator<T>
    {
        /// <summary>
        /// Validates specific value. 
        /// </summary>
        /// <param name="fieldValue">Value, that comes from somewere.</param>
        /// <returns>Error list (empty list if no errors)</returns>
        Task<IEnumerable<string>> ValidateAsync(T fieldValue);
    }
}
