﻿using DP.ClientServerInteraction.Common.ResponseModel;
using System.Threading.Tasks;

namespace DP.ClientServerInteraction.Common.Interfaces.Validators
{
    /// <summary>
    /// Model validator.
    /// </summary>
    /// <typeparam name="TRequestModel">Type of the request model.</typeparam>
    public interface IRequestModelValidator<TRequestModel>
    {
        /// <summary>
        /// Validates request.
        /// </summary>
        /// <param name="request">Model to validate.</param>
        /// <returns>Validation result. If no error was added to result - model is valid.</returns>
        Task<ModelValidationResult> ValidateAsync(TRequestModel request);
    }
}
