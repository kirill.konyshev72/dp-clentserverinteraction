﻿using DP.ClientServerInteraction.Common.ResponseModel;

namespace DP.ClientServerInteraction.Common
{
    public static class AjaxResultFactory
    {
        public static AjaxResult Create(AjaxResultType resultType, string message = null)
        {
            return new AjaxResult
            {
                ResultType = resultType,
                Message = message
            };
        }

        public static AjaxResult<T> Create<T>(AjaxResultType resultType, T result, string message = null)
        {
            return new AjaxResult<T>
            {
                ResultType = resultType,
                Message = message,
                ResultObject = result
            };
        }
    }
}
