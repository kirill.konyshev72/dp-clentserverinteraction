﻿namespace DP.ClientServerInteraction.Common.ResponseModel
{
    public class AjaxResult
    {
        public AjaxResultType ResultType { get; set; }
        public string Message { get; set; }

        public bool IsSuccess => ResultType == AjaxResultType.Success;
    }

    public class AjaxResult<T> : AjaxResult
    { 
        public T ResultObject { get; set; }
    }
}
