﻿using System.Collections.Generic;

namespace DP.ClientServerInteraction.Common.ResponseModel
{
    public class ModelValidationError
    {
        public ModelValidationError(string fieldName)
        {
            FieldName = fieldName;
            ErrorMessages = new List<string>();
        }

        public ModelValidationError(string fieldName, string errorMessage)
        {
            FieldName = fieldName;
            ErrorMessages = new List<string> { errorMessage };
        }

        public ModelValidationError(string fieldName, IEnumerable<string> errorMessages)
        {
            FieldName = fieldName;
            ErrorMessages = new List<string>(errorMessages);
        }

        public string FieldName { get; }
        public List<string> ErrorMessages { get; }
    }
}
