﻿using System.Collections.Generic;

namespace DP.ClientServerInteraction.Common.ResponseModel
{

    public class PaginationPage<T>
    {
        public IEnumerable<T> Page { get; }
        public int PageNumber { get; }
        public int TotalCount { get; }
         
        public PaginationPage(IEnumerable<T> page, int pageNumber, int totalCount)
        {
            Page = page;
            PageNumber = pageNumber;
            TotalCount = totalCount;
        }
    }
}
