﻿namespace DP.ClientServerInteraction.Common.ResponseModel
{
    public enum AjaxResultType
    {
        Fail = 0,
        Success = 1,
        ValidationError = 2, 
    }
}
