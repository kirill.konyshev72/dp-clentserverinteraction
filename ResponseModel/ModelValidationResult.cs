﻿using System.Collections.Generic;
using System.Linq;

namespace DP.ClientServerInteraction.Common.ResponseModel
{
    public class ModelValidationResult
    {

        public List<ModelValidationError> Errors { get; }

        public ModelValidationResult()
        {
            Errors = new List<ModelValidationError>();
        }

        /// <summary>
        /// Adds validation erorr.
        /// </summary>
        public void AddError(string fieldName, string errorMessage)
        {
            var error = Errors.FirstOrDefault(x => x.FieldName == fieldName);

            if (error != null)
            {
                error.ErrorMessages.Add(errorMessage);
            }
            else
            {
                Errors.Add(new ModelValidationError(fieldName, errorMessage));
            }
        }

        /// <summary>
        /// Adds validation erorrs.
        /// </summary>
        public void AddErrors(string fieldName, IEnumerable<string> errorMessages)
        {
            var error = Errors.FirstOrDefault(x => x.FieldName == fieldName);

            if (error != null)
            {
                error.ErrorMessages.AddRange(errorMessages);
            }
            else
            {
                if (errorMessages.Any())
                {
                    Errors.Add(new ModelValidationError(fieldName, errorMessages));
                }
            }
        }

        /// <summary>
        /// No errors, means model is valid.
        /// </summary>
        public bool IsValid => Errors.Count == 0;
    }

}
